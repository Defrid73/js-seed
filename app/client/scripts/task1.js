/*var total = [1,2,3,4].reduce(function(a, b) {
  return a * b;
});*/
function reduce (arr, fn){
    var prev = arr[0];
    for(var i=1; i<arr.length-1; i++){
       prev = fn(prev,arr[i]);
    }
    return fn(prev,arr[arr.length-1]);
}
var result = reduce([1, 2, 3, 4], function(a, b) {
  return a * b;
});
console.log(result);